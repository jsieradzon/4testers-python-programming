def calculate_square_of_the_number(input_number):
    return input_number * input_number


def celsius_to_fahrenheit(temperature_celsius):
    return temperature_celsius * 1.8 + 32


def calculate_the_volume_of_the_square(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # Zadanie 1
    print(calculate_square_of_the_number(0))
    print(calculate_square_of_the_number(16))
    print(calculate_square_of_the_number(2.55))

    # Zadanie 2
    print(celsius_to_fahrenheit(20))

    # Zadanie 3
    print(calculate_the_volume_of_the_square(2, 5, 7))
   
