import random
from datetime import date

# Lists of data
female_names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_names = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def get_random_list_element(list_param):
    return random.choice(list_param)


def generate_email_address(name, surname):
    return f"{name.lower()}.{surname.lower()}@example.com"


def get_random_age():
    random_age = random.randint(5, 45)
    return random_age


def is_person_an_adult(age):
    if age >= '18':
        return True
    else:
        return False


def calculate_the_birth_year(age):
    return date.today().year - age


def get_dictionary_with_person_data(names_list, surnames_list, countries_list):
    person_name = get_random_list_element(names_list)
    person_surname = get_random_list_element(surnames_list)
    person_age = get_random_age()
    return {
        "firstname": person_name,
        "lastname": person_surname,
        "country": get_random_list_element(countries_list),
        "email": generate_email_address(person_name, person_surname),
        "age": person_age,
        "adult": is_person_an_adult(person_age),
        "birth_year": calculate_the_birth_year(person_age)
    }


if __name__ == '__main__':
    print(
        f"Hi! I'm {['firstname']} {['lastname']}. I come from {['country']} and I was born in {['birth_year']}.")
