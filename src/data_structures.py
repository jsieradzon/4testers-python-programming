movies = ['A', 'B', 'C', 'D', 'E']

last_movie = movies[-1]
movies.append('F')
movies.append('G')
print(len(movies))
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'H')
print(movies)

email = ['a@example.com', 'b@example.com']
print(len(email))
print(email[0])
print(email[-1])
email.append('cde@example.com')

friend = {
    "name": "Damian",
    "age": 40,
    "hobbys": ["computers", "motorcycles"]
}

friend_hobbies = friend["hobby"]
print("Hobbies of my friend:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")
friend["hobby"].append('football')
print(friend)
