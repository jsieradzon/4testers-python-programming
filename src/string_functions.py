def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widzień w naszym mieście: {city}!")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Asia", "Poznań")
    print_greetings_for_a_person_in_the_city("Damian", "Kraków")


def generate_email_for_4testers(first_name, last_name):
    email_name = first_name.lower()
    email_surname = last_name.lower()
    print(f"{email_name}.{email_surname}@4testers.pl")


if __name__ == '__main__':
    generate_email_for_4testers("Janusz", "Nowak")
    generate_email_for_4testers("Barbara", "Kowalska")
