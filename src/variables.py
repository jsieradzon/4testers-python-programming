first_name = "Asia"
last_name = "Sieradzon"
age = 33

print(first_name)
print(last_name)
print(age)

# I am defining a set of variables describing my best friend
first_name = "Damian"
age = 40
numbers_of_animals = 1
he_has_driving_license = True
acquaintance_in_years = 15.5

print(first_name, age, numbers_of_animals, he_has_driving_license, acquaintance_in_years, sep=',')




