from random import randint


def print_hello_40_times():
    for i in range(1, 41):
        print('Hello!', i)


def print_positive_numbers_divisible_by_7(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)


def print_n_random_numbers(n):
    for i in range(n):
        print(f'loop {i}: ', randint(1, 1000))


if __name__ == '__main__':
    print_hello_40_times()
    print_positive_numbers_divisible_by_7(1, 30)


