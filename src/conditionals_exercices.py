def calcutale_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print('Your speed was:', speed)
    if speed > 100:
        print('You just lost your driving license :(')
    elif speed > 50:
        fine_amount = 500 + (speed - 50) * 10
        print(f'You just got a fine! Fine amount {calcutale_fine_amount} :)')
    else:
        print('Thank you, your speed is fine')


def display_speed_information(speed):
    if speed > 50:
        print('Slow down! :(')

    else:
        print('Thank you, your speed is below limit! :)')


def check_if_conditions_are_normal(temperature_in_celsius, pressure_in_hectopascals):
    return True if temperature_in_celsius == 0 and pressure_in_hectopascals == 1013 else False


if __name__ == '__main__':
    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(50)
    print_the_value_of_speeding_fine_in_built_up_area(51)

if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

if __name__ == '__main__':
    print(check_if_conditions_are_normal(0, 1013))
    print(check_if_conditions_are_normal(1, 1013))
    print(check_if_conditions_are_normal(0, 1014))
    print(check_if_conditions_are_normal(1, 1014))


def grade_info(grade):
    if not (isinstance(grade, float) or isinstance(grade, int)):
        return ('N/A')
    elif grade >= 4.0:
        return 'dobry'
    elif grade >= 3.0:
        return 'dostateczny'
    elif grade >= 2.0:
        return 'niedostateczny'
    else:
        return ('N/A')


print(grade_info(5))
print(grade_info(4.5))
print(grade_info(4))
print(grade_info(3))
print(grade_info(2))
print(grade_info(1))
print(grade_info(6))
